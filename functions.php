<?php

/**
 * WordPress Offers.
 *
 * @since 1.0.0
 * @package WordPress Offers
 * @copyright 2021 Lacey Tech
 * @link https://lacey-tech.com
 */


 //Custom admin Colums copied from Rugmart Offers
add_filter('manage_lts_offer_posts_columns', 'set_custom_edit_offers_columns');
function set_custom_edit_offers_columns($columns)
{
    unset($columns['date']);
    $columns['offer_type'] = __('Offer Type');
    // TODO: Commented for next phase
    // $columns['start_date'] = __('Start Date');
    // $columns['end_date'] = __('End Date');
    $columns['last_updated'] = __('Last Updated');
    $columns['occasions'] = __('Occasions');
    $columns['status'] = __('Status');
    return $columns;
}

//Add the data to the custom columns for the offers post type:
    add_action('manage_lts_offer_posts_custom_column', 'custom_offers_column', 10, 2);
    function custom_offers_column($column, $post_id)
    { 
       
        switch ($column) {
            case 'offer_type':
                $offer_type = get_post_meta($post_id, OFFERS_META_PREFIX . 'type', true);
                //echo ucwords(str_replace([' '], ['-'],$offer_type));
                echo strtoupper($offer_type);
                break;
                 // TODO: Commented for next phase
            case 'start_date':
                echo date(PUBLIC_DATE_FORMAT, strtotime(get_post_meta($post_id, OFFERS_META_PREFIX . 'start_date', true)));
                break;
            case 'end_date':
                echo date(PUBLIC_DATE_FORMAT, strtotime(get_post_meta($post_id, OFFERS_META_PREFIX . 'end_date', true)));
                break;
            case 'last_updated':
                echo get_the_modified_time(PUBLIC_DATE_FORMAT);
                break;
            case 'occasions':
                $terms = get_the_term_list($post_id, 'occasions', '', '<br>', '');
    
                if (is_string($terms)) {
                    echo $terms;
                } else {
                    _e('No Occasions Set');
                }
    
                break;
            case 'status':
                // TODO: Commented for next phase
                // $todaydate = date('Y-m-d');
                // $today=date(DATE_FORMAT, strtotime($todaydate));
                // $status = ($today < date(DATE_FORMAT, strtotime(get_post_meta($post_id, OFFERS_META_PREFIX . 'end_date', true))) && $today > date(DATE_FORMAT, strtotime(get_post_meta($post_id, OFFERS_META_PREFIX . 'start_date', true)))) ? '<span class="active">active</span>' : '<span class="inactive">inactive</span>';
                // echo $status;
                echo get_post_status($post_id);

                
                break;
        }
    }


    //Get offer fields for front end template
    function get_offer_fields_for_template($offer){
        $offer_id = $offer['ID'];
    
        $offer_fields_array = [
            OFFERS_META_PREFIX . 'id' => $offer_id,
            OFFERS_META_PREFIX . 'type' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'type', true),
            OFFERS_META_PREFIX . 'headline' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'headline', true),
            OFFERS_META_PREFIX . 'description' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'description', true),
            OFFERS_META_PREFIX . 'rug_page_url' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'rug_page_url', true),
            OFFERS_META_PREFIX . 'buy_online_url' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'buy_online_url', true),
            OFFERS_META_PREFIX . 'button_text' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'button_text', true),
            OFFERS_META_PREFIX . 'single_image' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'single_image', true),
            OFFERS_META_PREFIX . 'text_colour' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'text_colour', true),
            OFFERS_META_PREFIX . 'background_colour' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'background_colour', true),
            OFFERS_META_PREFIX . 'visual_styles' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'visual_styles', true),
            OFFERS_META_PREFIX . 'slides' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'slides', true),
            OFFERS_META_PREFIX . 'start_date' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'start_date', true),
            OFFERS_META_PREFIX . 'end_date' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'end_date', true),
            // OFFERS_META_PREFIX . 'show_on_rugs' => get_subpage_ids('rugs', get_post_meta($offer_id, OFFERS_META_PREFIX . 'show_on_rugs', true)),
            // OFFERS_META_PREFIX . 'hide_on_rugs' => get_subpage_ids('rugs', get_post_meta($offer_id, OFFERS_META_PREFIX . 'hide_on_rugs', true)),
            OFFERS_META_PREFIX . 'show_on_product_cats' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'show_on_product_cats', true),
            OFFERS_META_PREFIX . 'hide_on_product_cats' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'hide_on_product_cats', true),
        ];
    
        return $offer_fields_array;
    }


    
/**
 * Generate a styles string for elements
 *
 * @param array<string,mixed> $offer_fields
 * @return string
 */
function get_styles($offer_fields, $groups=[]){
    $styles_string = '';
    
    // Deal with the styles from the format field type
    $value = $offer_fields[OFFERS_META_PREFIX . 'visual_styles'];
    foreach( $groups as $content_wrap_group ):
        if( isset( $value[$content_wrap_group . '_all'] ) && ! empty( $value[$content_wrap_group . '_all'] ) ):
            $styles_string .= $content_wrap_group . ': ' . $value[$content_wrap_group . '_all'] . ';';
        else:
            if( isset( $value[$content_wrap_group . '_top'] ) && ! empty( $value[$content_wrap_group . '_top'] ) ):
                $styles_string .= $content_wrap_group . '-top: ' . $value[$content_wrap_group . '_top'] . ';';
            endif;

            if( isset( $value[$content_wrap_group . '_right'] ) && ! empty( $value[$content_wrap_group . '_right'] ) ):
                $styles_string .= $content_wrap_group . '-right: ' . $value[$content_wrap_group . '_right'] . ';';
            endif;

            if( isset( $value[$content_wrap_group . '_bottom'] ) && ! empty( $value[$content_wrap_group . '_bottom'] ) ):
                $styles_string .= $content_wrap_group . '-bottom: ' . $value[$content_wrap_group . '_bottom'] . ';';
            endif;

            if( isset( $value[$content_wrap_group . '_left'] ) && ! empty( $value[$content_wrap_group . '_left'] ) ):
                $styles_string .= $content_wrap_group . '-left: ' . $value[$content_wrap_group . '_left'] . ';';
            endif;
        endif;
    endforeach;

    // Border color
    if( isset( $value['border_color'] ) && ! empty( $value['border_color'] ) ) {
        $styles_string .=  'border-color: ' . $value['border_color'] . ';';
    }

    // Border style
    if( isset( $value['border_style'] ) && ! empty( $value['border_style'] ) ) {
        $styles_string .=  'border-style: ' . $value['border_style'] . ';';
    }

    // Border radius
    if( isset( $value['border_radius'] ) && ! empty( $value['border_radius'] ) ) {
        $styles_string .=  'border-radius: ' . $value['border_radius'] . ';';
    }
    
    // Make the styles string readable by adding a space after each CSS property and trimming any whitespace at the end
    $styles_string = trim(str_replace(';', '; ', $styles_string));
    
    return $styles_string;
}


function get_custom_styles($offer_fields, $style){
    $styles_string = '';

    // If the offer has a background property then add it to the styles_string
    if( $style == 'background' ): 
        $background_styles = isset($offer_fields[OFFERS_META_PREFIX . 'background_colour']) ? "background: {$offer_fields[OFFERS_META_PREFIX . 'background_colour']};" : "";
        $styles_string = $background_styles;
    endif;

    // If the offer has a text colour property then add it to the styles_string
    if( $style == 'text' ): 
        $text_styles = isset($offer_fields[OFFERS_META_PREFIX . 'text_colour']) ? "color: {$offer_fields[OFFERS_META_PREFIX . 'text_colour']};" : "";
        $styles_string = $text_styles; 
    endif;

    // Make the styles string readable by adding a space after each CSS property and trimming any whitespace at the end
    $styles_string = trim(str_replace(';', '; ', $styles_string));

    return $styles_string;
}

function returnchildCategories($parent_category_ids = [])
{
    $child_rugcategories = [];
    foreach($parent_category_ids as $parent_category_id){
        $child_rug_categories= get_categories(array(
            'taxonomy' => 'product_cat',
            'parent'  => $parent_category_id
        ));
    
        if (!empty($child_rug_categories)) {
            foreach ($child_rug_categories as $childrugcategory) {
    
                array_push($child_rugcategories, $childrugcategory->name);
            }
        } else {
            //echo 'Itself a child rug';
        }
    }
  
    return $child_rugcategories;
}