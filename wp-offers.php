<?php

/**
 * WordPress Offers.
 *
 * @since 1.0.0
 * @package WordPress Offers
 * @copyright 2021 Lacey Tech
 * @link https://lacey-tech.com
 *
 * @wordpress-plugin
 * Plugin Name: WordPress Offers
 * Description: Display and control the offers displayed on your website.
 * Version: 1.0.0
 * Author: Lacey Tech
 * Author URI: https://lacey-tech.com
 * License: GPL-2.0+
 * License URI: http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain: lts-wp-offers
 */

use LTS\Offers\{
    Metabox,
    PostType,
    Display
};

// Deny direct access
if (!defined('WPINC')) {
    exit;
}

// Plugin constants
define('OFFERS_META_PREFIX', 'lts_offer_');
define('OFFERS_POST_TYPE', 'lts_offer');
define('OFFERS_DISPLAY_DATETIME_FORMAT', 'dS F Y');
define('OFFERS_DISPLAY_LIMIT', 5);
define('DATE_FORMAT', 'Y-m-d');
define('PUBLIC_DATE_FORMAT', 'dS F Y');
define('DATE_TIME_FORMAT', 'Y-m-d H:i:s');
define('PLACEHOLDER_IMAGE', 'https://rugmart.co.uk/wp-content/uploads/2020/07/cabinet-ceiling-clean-comfort-1669799-scaled-600x401.jpg');

// Load common functions
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'functions.php']);

// Load plugin files
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'src', 'Metabox.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'src', 'PostType.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'src', 'Display.php']);

// Autoload CMB2
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'vendor', 'cmb2', 'init.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'vendor', 'cmb2-field-post-search-ajax', 'cmb-field-post-search-ajax.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'vendor', 'cmb2-conditionals', 'cmb2-conditionals.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'vendor', 'cmb2-field-visual-style-editor', 'cmb2-field-visual-style-editor.php']);
require_once join(DIRECTORY_SEPARATOR, [__DIR__, 'vendor', 'cmb-field-select2', 'cmb-field-select2.php']);

// Init as necessary
// $display = null;
$metabox = new Metabox(
    function () use (&$display) {
        // Circular reference work-around
        echo $display->render(time());
    },
    OFFERS_META_PREFIX
);
$post_type = (function () use (&$metabox) {
    return new PostType(
        $metabox,
        OFFERS_POST_TYPE
    );
})();
$display = new Display(
    $post_type,
    OFFERS_DISPLAY_LIMIT,
    OFFERS_DISPLAY_LIMIT,
    OFFERS_DISPLAY_DATETIME_FORMAT
);

add_action(
    'init',
    /**
     * Register the offers system post type(s) and taxonomies.
     *
     * @since 1.0.0
     *
     * @return void
     */
    function () use ($post_type) {
        // Register
        $post_type->register();
    },
    100
);

add_action(
    'cmb2_admin_init',
    /**
     * Initialise metabox registration.
     *
     * @since 1.0.0
     *
     * @return void
     */
    function () use ($post_type, $metabox) {
        // Configure the CMB2 boxes
        $details_box = new_cmb2_box([
            'id' => $metabox->addPrefix('detail'),
            'title' => __('Offer Details'),
            'object_types' => [
                $post_type->getPostType()
            ],
            'context' => 'advanced',
            'priority' => 'high',
        ]);

        // Configure the CMB2 boxes
        $display_options_box = new_cmb2_box([
            'id' => $metabox->addPrefix('display_options'),
            'title' => __('Offer Display options'),
            'object_types' => [
                $post_type->getPostType()
            ],
            'context' => 'advanced',
            'priority' => 'high',
        ]);
        //Removing Offer Preview Box
        // $preview_box = new_cmb2_box([
        //     'id' => $metabox->addPrefix('preview'),
        //     'title' => __('Offer Preview'),
        //     'object_types' => [
        //         $post_type->getPostType()
        //     ],
        //     'context' => 'advanced',
        //     'priority' => 'default',
        // ]);

        $styles_box = new_cmb2_box([
            'id' => $metabox->addPrefix('style'),
            'title' => __('Offer Styles'),
            'object_types' => [
                $post_type->getPostType()
            ],
            'context' => 'advanced',
            'priority' => 'low',
        ]);

        // Register the structure
        $metabox->register(

            $details_box,
            $display_options_box,
            // $preview_box, //Removing Offer Preview Box
            $styles_box
        );
    }
);
/**
     * Display the offer system of posts.
     *
     * @since 1.0.0
     *
     * @return void
     */
add_action(
    'astra_entry_top',
    function () use ($display) {
        //var_dump("offers");
        $markup = $display->render(
            time(),
            get_the_ID()
        );
        echo $markup;
    }
);

/**
     * Display the offer system of categories.
     *
     * @since 1.0.0
     *
     * @return void
     */
add_action('woocommerce_before_main_content',
function ()  use ($display)
{  if (is_product_category()){
 
     //var_dump("offers");
     $markup = $display->render(
        time(),
        get_the_ID()
    );
    echo $markup;
}
    
}
);

/**
     * Display the offer system of products.
     *
     * @since 1.0.0
     *
     * @return void
     */
    add_action('woocommerce_after_single_product_summary',
    function ()  use ($display)
    {  if ( is_product()){
     
         //var_dump("offers");
         $markup = $display->render(
            time(),
            get_the_ID()
        );
        echo $markup;
    }
        
    }
    );

add_shortcode(
    'offers',
    function () use ($display) {
        $markup = $display->renderShortcode(
            time()
        );
        return $markup;
    }
);



add_action(
    'wp_enqueue_scripts',
    /**
     * Offers system scripts and styles enqueuing.
     *
     * @since 1.0.0
     *
     * @return void
     */
    function () {
        // Offers styling
        wp_enqueue_script(
            'offers-scripts',
            plugins_url(
                join(
                    DIRECTORY_SEPARATOR,
                    [
                        'offers_v3',
                        'assets',
                        'js',
                        'script.js'
                    ]
                )
            )
        );
    }
);

add_action(
    'wp_enqueue_scripts',
    /**
     * Offers system scripts and styles enqueuing.
     *
     * @since 1.0.0
     *
     * @return void
     */
    function () {
        // Offers styling
        wp_enqueue_style(
            'offers-styles',
            plugins_url(
                join(
                    DIRECTORY_SEPARATOR,
                    [
                        'offers_v3',
                        'assets',
                        'css',
                        'offers.css'
                    ]
                )
            )
        );
    }
);
