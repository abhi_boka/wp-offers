

<?php
    // Get The Offer Fields
    $offer_fields = get_offer_fields();

    // Get The Offer Styles
    $offer_styles = get_styles($offer_fields, $groups=['margin', 'border', 'padding']);
    $text_styles = get_custom_styles($offer_fields, 'text');
    $background_styles = get_custom_styles($offer_fields, 'background');
    $offer_styles = $offer_styles .= $background_styles;

    // Store The Data In Local Variables
    $slides = isset($offer_fields[PREFIX . 'slides']) ? esc_html(unserialize($offer_fields[PREFIX . 'slides'])) : '';
?>

<?php if($slides): ?>
    <div class="offer image-slider owl-carousel">
        <?php foreach($slides as $slide): ?>

            <?php
              echo '<pre>' . var_dump($slide) . '</pre>';
            ?>

            <div class="slide">
                <?php if(isset($website_link)): ?><a href="<?php echo $$website_link; ?>"><?php endif; ?>
                    <div class="image">
                        <img src="<?php echo $slide_image; ?>" alt="<?php echo esc_html($discount_title); ?>" />            
                    </div>
                    
                    <div class="text">
                        <?php if(isset($discount)): ?><div class="sale-banner"><?php echo $discount; ?></div><?php endif; ?>
                        <?php if(isset($title)): ?><div class="range"><?php echo $title; ?></div><?php endif; ?>
                        <?php if(isset($offer_ends)): ?><div class="offer-ends"><?php echo $offer_ends; ?></div><?php endif; ?>
                    </div>
                <?php if(isset($website_link)): ?></a><?php endif; ?>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>


<?php // Load Owl Carousel in the view and include it in the offers plugin JS files ?>