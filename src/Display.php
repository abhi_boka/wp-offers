<?php

/**
 * WordPress Offers.
 *
 * @since 1.0.0
 * @package WordPress Offers
 * @copyright 2021 Lacey Tech
 * @link https://lacey-tech.com
 */

namespace LTS\Offers;

class Display
{
    /**
     * The offers post type.
     *
     * @since 1.0.0
     *
     * @var PostType
     */
    protected $post_type;

    /**
     * The meta prefix.
     *
     * @since 1.0.0
     *
     * @var string
     */
    protected $prefix;

    /**
     * The maximum number of displayed offers.
     *
     * @since 1.0.0
     *
     * @var int
     */
    protected $limit;

    /**
     * The display datetime format.
     *
     * @since 1.0.0
     *
     * @var string
     */
    protected $datetime_format;

    /**
     * Constructor.
     *
     * @since 1.0.0
     *
     * @param int $limit
     */
    function __construct(
        PostType $post_type,
        string $prefix,
        int $limit = 5,
        string $datetime_format = 'dS F Y'
    ) {
        $this->post_type = $post_type;
        $this->prefix = $prefix;
        $this->limit = $limit;
        $this->datetime_format = $datetime_format;
    }

    /**
     * Generate an inline styles string.
     * Takes the visual styles data and converts it into sectioned inline
     * styles according to the style sections/groups selected.
     *
     * @since 1.0.0
     *
     * @param array|null $visual_styles
     * @param array $groups
     * @return string|null
     */
    protected function inlineStyles(
        ?array $visual_styles = null,
        array $groups
    ): ?string {
        if ($visual_styles === null) {
            return null;
        }

        $inline = [];

        $groups = array_intersect_key(
            array_flip($groups),
            $visual_styles
        );

        $sided_groups = [
            'margin',
            'border',
            'padding'
        ];
        $sides = [
            'top',
            'right',
            'bottom',
            'left'
        ];

        // Handle style groups with sides (margin, border, padding)
        foreach ($sided_groups as $group) {
            $found_in_group = false;

            $tmp_sides = $sides;
            $tmp_sides[] = 'all';

            foreach ($tmp_sides as $side) {
                if (array_key_exists($group . '_' . $side, $groups)) {
                    $found_in_group = true;
                    break;
                }
            }

            if (!$found_in_group) {
                continue;
            }

            if (
                isset($visual_styles[$group . '_all'])
                && !empty($visual_styles[$group . '_all'])
            ) {
                $inline[] = $group . ': ' . $visual_styles[$group . '_all'];
                continue;
            }


            foreach ($sides as $side) {
                if (
                    isset($visual_styles[$group . '_' . $side])
                    && !empty($visual_styles[$group . '_' . $side])
                ) {
                    $inline[] = "{$group}-{$side}: " . $visual_styles[$group . '_' . $side];
                }
            }
        }

        $individual_groups = [
            'text_color' => 'color',
            'background_color' => 'background-color',
            'border_color' => 'border-color',
            'border_style' => 'border-style',
            'border_radius' => 'border-radius',
        ];

        foreach ($individual_groups as $group => $style_key) {
            if (
                isset($visual_styles[$group])
                && !empty($visual_styles[$group])
            ) {
                $inline[] = "{$style_key}: " . $visual_styles[$group];
            }
        }

        return count($inline) > 0 ? join('; ', $inline) . ';' : null;
    }

    /**
     * Render the offers display.
     *
     * @since 1.0.0
     *
     * @param int $timestamp
     * @return string|null
     */
    public function render(int $timestamp, ?int $page_id = null): ?string
    {
        $markup = '
        <div class="slideshow-container">';

        $offers = $this->post_type->getActive(
            $timestamp,
            $page_id,
            $this->limit
        );

        //print_r($offers);
        // var_dump($offers);

        // Inline styles order:
        // 1. Container/Visual styles
        // 2. Text styles
        // 3. Background styles
        //print_r($offers);
        if (empty($offers)) {
            //echo 'Offers is empty on this page';  //commented for debugging
        } else {
       
            foreach ($offers as $offer) {

                if ($offer['type'] === 'text') {

                    $markup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';

                    continue;
                } else if ($offer['type'] === 'text-image') {
                    $markup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';
                    continue;
                } else if ($offer['type'] === 'single-image') {
                    $markup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';
                    continue;
                } else {
                    continue;
                }

                // TODO: Add support for multiple offers to display
                break;
            }
            $markup .= '
            
            </div>
            <div style="text-align:center;margin-top:20px">
            ';
            if(count($offers) > 1){
                $markup .= '<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a> ';
            }else{
                $markup .= '';
            }
       
            $markup .= '</div>
            <br>
            <script>
                        var slideIndex = 1;
                        showSlides(slideIndex);

                        function plusSlides(n) {
                            showSlides(slideIndex += n);
                        }

                        function currentSlide(n) {
                            showSlides(slideIndex = n);
                        }

                        function showSlides(n) {
                            var i;
                            var slides = document.getElementsByClassName("mySlides");

                            if (n > slides.length) { slideIndex = 1 }
                            if (n < 1) { slideIndex = slides.length }
                            for (i = 0; i < slides.length; i++) {
                                slides[i].style.display = "none";
                            }

                            slides[slideIndex - 1].style.display = "block";

}
            </script>';
        }


        return $markup;
    }


     /**
     * Render the offers display for shortcode.
     *
     * @since 1.0.0
     *
     * @param int $timestamp
     * @return string|null
     */
    public function renderShortcode(int $timestamp, ?int $page_id = null): ?string
    {
        $offermarkup = '
        <div class="slideshow-container">';

        $offers = $this->post_type->getActiveShortcode(
            $timestamp,
            $page_id,
            $this->limit
        );

        //print_r($offers);
        // var_dump($offers);

        // Inline styles order:
        // 1. Container/Visual styles
        // 2. Text styles
        // 3. Background styles
        //print_r($offers);
        if (empty($offers)) {
            //echo 'Offers is empty on this page';  //commented for debugging
        } else {
       
            foreach ($offers as $offer) {

                if ($offer['type'] === 'text') {

                    $offermarkup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';

                    continue;
                } else if ($offer['type'] === 'text-image') {
                    $offermarkup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';
                    continue;
                } else if ($offer['type'] === 'single-image') {
                    $offermarkup .= '<div class="mySlides fade">' . $this->load_offer_template($offer) . '</div>';
                    continue;
                } else {
                    continue;
                }

                // TODO: Add support for multiple offers to display
                break;
            }
            $offermarkup .= '
            
            </div>
            <div style="text-align:center;margin-top:20px">
            ';
            if(count($offers) > 1){
                $offermarkup .= '<a class="prev" onclick="plusSlides(-1)">&#10094;</a>
                <a class="next" onclick="plusSlides(1)">&#10095;</a> ';
            }else{
                $offermarkup .= '';
            }
       
            $offermarkup .= '</div>
            <br>
            <script>
                        var slideIndex = 1;
                        showSlides(slideIndex);

                        function plusSlides(n) {
                            showSlides(slideIndex += n);
                        }

                        function currentSlide(n) {
                            showSlides(slideIndex = n);
                        }

                        function showSlides(n) {
                            var i;
                            var slides = document.getElementsByClassName("mySlides");

                            if (n > slides.length) { slideIndex = 1 }
                            if (n < 1) { slideIndex = slides.length }
                            for (i = 0; i < slides.length; i++) {
                                slides[i].style.display = "none";
                            }

                            slides[slideIndex - 1].style.display = "block";

}
            </script>';
        }


        return $offermarkup;
    }

    protected function load_offer_template($offer)
    {
        $return = null;
        ob_start();

        $offer_fields_array = $this->get_offer_fields($offer);
        //print_r($offer_fields_array);
        $offer_type = $offer_fields_array[OFFERS_META_PREFIX . 'type'];

        if (isset($offer_type) && !empty($offer_type)) :
            switch ($offer_type):
                case 'single-image':
                    //echo '<br>Show template single-image.php<br/>';
                    include __DIR__ . '/views/single-image.php';
                    break;
                case 'text-image':
                    //echo '<br>Show template text-image.php<br/>';
                    include __DIR__ . '/views/text-image.php';
                    break;
                case 'text':
                    //echo '<br>Show template text.php<br/>';
                    include __DIR__ . '/views/text.php';
                    break;
                case 'image-slider':
                    //echo '<br>Show template image-slider.php<br/>';
                    include __DIR__ . '/views/image-slider.php';
                    break;
            endswitch;

            return ob_get_clean();
        endif;

        return $return;
    }

    protected function get_offer_fields($offer)
    {
        $offer_id = $offer['ID'];

        $offer_fields_array = [
            OFFERS_META_PREFIX . 'id' => $offer_id,
            OFFERS_META_PREFIX . 'type' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'type', true),
            OFFERS_META_PREFIX . 'headline' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'headline', true),
            OFFERS_META_PREFIX . 'description' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'description', true),
            OFFERS_META_PREFIX . 'link' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'link', true),
            OFFERS_META_PREFIX . 'button_text' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'button_text', true),
            OFFERS_META_PREFIX . 'single_image' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'single_image', true),
            OFFERS_META_PREFIX . 'text_colour' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'text_colour', true),
            OFFERS_META_PREFIX . 'background_colour' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'background_colour', true),
            OFFERS_META_PREFIX . 'visual_styles' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'visual_styles', true),
            OFFERS_META_PREFIX . 'slides' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'slides', true),
            OFFERS_META_PREFIX . 'start_date' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'start_date', true),
            OFFERS_META_PREFIX . 'end_date' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'end_date', true),
            // OFFERS_META_PREFIX . 'show_on_rugs' => get_subpage_ids('rugs', get_post_meta($offer_id, OFFERS_META_PREFIX . 'show_on_rugs', true)),
            // OFFERS_META_PREFIX . 'hide_on_rugs' => get_subpage_ids('rugs', get_post_meta($offer_id, OFFERS_META_PREFIX . 'hide_on_rugs', true)),
            OFFERS_META_PREFIX . 'show_on_product_cats' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'show_on_product_cats', true),
            OFFERS_META_PREFIX . 'hide_on_product_cats' => get_post_meta($offer_id, OFFERS_META_PREFIX . 'hide_on_product_cats', true),
        ];

        return $offer_fields_array;
    }


    protected function renderText(
        array $offer
    ): string {
        // var_dump($offer['visual_styles']);
        $prefix_style = function ($group) {
            return array_map(
                function ($side) use ($group) {
                    return $group . '_' . $side;
                },
                [
                    'all',
                    'top',
                    'right',
                    'bottom',
                    'left'
                ]
            );
        };

        $container_styles = $this->inlineStyles(
            $offer['visual_styles'],
            [
                // ...$prefix_style('margin'),
                // ...$prefix_style('border'),
                // ...$prefix_style('padding'),
                'background_color',
            ]
        );

        $text_styles = $this->inlineStyles(
            $offer['visual_styles'],
            [
                'text_color'
            ]
        );

        return sprintf(
            '
            <div class="offer text" style="%1$s">
                <div class="offer-inner">
                    <h3 class="headline" style="%2$s">
                        %3$s
                    </h3>

                    <div class="description style="%2$s">
                        %4$s
                    </div>

                    <div class="end-date" style="%2$s">
                        %5$s
                    </div>

                    <a href="%6$s" class="button primary" style="display: inline-block; margin-top: 10px;">
                        %7$s
                    </a>
                </div>
            </div>
            ',
            $container_styles,
            $text_styles,
            $offer['headline'],
            $offer['description'],
            date(
                $this->datetime_format,
                (int) $offer['end_date']
            ),
            $offer['link'],
            $offer['button_text']
        );
    }
}
