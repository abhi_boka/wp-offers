'use strict';

import gulp from 'gulp';
import print from 'gulp-print';
import rename from 'gulp-rename';
import gulpSass from 'gulp-sass';
import dartSass from 'sass';
import autoprefixer from 'autoprefixer';
import cssnano from 'cssnano';
import postcss from 'gulp-postcss';

/**
 * Styles and Sass compilation.
 *
 * @since 1.0.0
 *
 * @author Dom Webber <dom.webber@hotmail.com>
 */
function compileStyles() {
  const gulpSassInit = gulpSass(dartSass);

  // Define the PostCSS plugins for both expanded and minfied versions
  const postcss_plugins_all = [
    autoprefixer(),
  ];

  // Customise PostCSS plugins for expanded versions
  const postcss_plugins_expanded = [
    ...postcss_plugins_all,
    ...[],
  ];

  // Customise PostCSS plugins for minified versions
  const postcss_plugins_minified = [
    ...postcss_plugins_all,
    ...[
      cssnano(
        {
          zindex: false,
        }
      ),
    ],
  ];

  return gulp.src(
    './assets/**/*.scss'
  )

  // Compile the SASS source
  .pipe(
    gulpSassInit(
      {
        outputStyle: 'expanded',
      }
    )
  )

  // Adjust the expanded distribution output
  .pipe(
    postcss(
      postcss_plugins_expanded,
    ),
  )

  // Save the expanded distribution output
  .pipe(
    gulp.dest(
      './dist'
    )
  )

  // Console log the output filename
  .pipe(
    print(),
  )

  // Apply the minification process
  .pipe(
    postcss(
      postcss_plugins_minified,
    ),
  )

  // Rename the minified outputs to .min.css
  .pipe(
    rename(
      {
        extname: '.min.css',
      },
    ),
  )

  // Console log the minified filenames
  .pipe(
    print()
  )

  // Save the minified distribution output
  .pipe(
    gulp.dest(
      './dist'
    )
  );
}

export { compileStyles };
export default gulp.series(compileStyles);
