

<?php

    // Get The Offer Fields
    $offer_fields = get_offer_fields_for_template($offer);
    //echo '<pre>' . $offer_fields . '</pre>';

    // Get The Offer Styles
    $offer_styles = get_styles($offer_fields, $groups=['margin', 'border', 'padding']);
    $text_styles = get_custom_styles($offer_fields, 'text');
    $background_styles = get_custom_styles($offer_fields, 'background');
    $offer_styles = $offer_styles .= $background_styles;

    // Store The Data In Local Variables
    $headline = isset($offer_fields[OFFERS_META_PREFIX . 'headline']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'headline']) : '';
    $description = isset($offer_fields[OFFERS_META_PREFIX . 'description']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'description']) : '';
    $end_date = isset($offer_fields[OFFERS_META_PREFIX . 'end_date']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'end_date']) : '';
    $rug_page_url = isset($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) : '';
    $buy_online_url = isset($offer_fields[OFFERS_META_PREFIX . 'buy_online_url']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'buy_online_url']) : '';
    $button_text = isset($offer_fields[OFFERS_META_PREFIX . 'button_text']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'button_text']) : 'Get Offer';
?>

<div class="offer text-only " style="<?php if(isset($offer_styles)): echo $offer_styles; endif; ?>;margin-top: 30px">
    <div class="offer-inner" style="padding: 10px;text-align: center;">

        <h3 class="headline" style="<?php if(isset($text_styles)): echo $text_styles; endif; ?>;text-transform: uppercase;font-size: 30px;">
            <?php echo $headline; ?>
        </h3>

        <div class="description" style="<?php if(isset($text_styles)): echo $text_styles; endif; ?>;font-size: 13px;">
            <?php echo $description; ?>
        </div>
        <!-- TODO : End date functionality -->
        <!-- <div class="end-date" style="<?php if(isset($text_styles)): echo esc_html($text_styles); endif; ?>">
            <?php echo $end_date; ?>
        </div> -->
        <!-- Button if exists then show -->
        <?php if(!empty($rug_page_url)){?>
           <a href="<?php echo $rug_page_url; ?>"><button  class="button primary">Showroom Rugs</button></a>
        <?php } ?>
        <?php if(!empty($buy_online_url)){?>
            <a href="<?php echo $buy_online_url; ?>"><button class="button primary">Buy Online</button></a>
        <?php } ?>
      
    </div>
</div>