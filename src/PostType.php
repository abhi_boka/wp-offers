<?php

/**
 * WordPress Offers.
 *
 * @since 1.0.0
 * @package WordPress Offers
 * @copyright 2021 Lacey Tech
 * @link https://lacey-tech.com
 */

namespace LTS\Offers;

use WP_Query;
use WP_Post;

use function PHPSTORM_META\map;

/**
 * Setup for the offers post type.
 *
 * @since 1.0.0
 * @author Dom Webber <dom.webber@hotmail.com>
 */
class PostType
{
    /**
     * The metabox handler.
     *
     * @since 1.0.0
     *
     * @var Metabox
     */
    protected $metabox;

    /**
     * The post type name.
     *
     * @since 1.0.0
     *
     * @var string
     */
    protected $post_type;

    /**
     * Constructor.
     *
     * @since 1.0.0
     */
    function __construct(
        Metabox $metabox,
        string $post_type = 'offer'
    ) {
        $this->metabox = $metabox;
        $this->post_type = $post_type;
    }

    /**
     * Retrieve the post type name.
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function getPostType(): string
    {
        return $this->post_type;
    }

    /**
     * Format the offer into a standardized format.
     * Resolves the post and post metadata into a standard format
     * for use in display and other system areas.
     *
     * @since 1.0.0
     *
     * @param WP_Post $offer
     * @param array $meta
     * @return array
     */
    protected function formatOffer(WP_Post $offer, array $meta): array
    {
        // Directly accessible data (such as the WP Post ID)
        $formatted = [
            'ID' => $offer->ID
        ];

        // TODO: Consider moving to the Metabox class
        $meta_keys = [
            'type',
            'headline',
            'description',
            'start_date',
            'end_date',
            'include_on_rugs',
            'exclude_on_rugs',
            'include_on_categories',
            'exclude_on_categories',
            'link',
            'button_text',
            'slides',
            'text_colour',
            'background_colour',
            'visual_styles'
        ];

        $prefixed_keys = array_flip(
            array_map(
                function ($key) {
                    return $this->metabox->addPrefix($key);
                },
                $meta_keys
            )
        );

        // Flip-Flop stuff :)
        $intersection = array_intersect_key(
            $meta,
            $prefixed_keys
        );

        // Ensure all keys exist, even if their values are null
        $diff = array_map(
            function () {
                // Array containing null for reset()
                return [null];
            },
            array_diff_key(
                $prefixed_keys,
                $meta
            )
        );

        // Merge the cherry-picked keys with the null defaults
        $inter_diff_merge = $intersection + $diff;

        foreach ($inter_diff_merge as $key => $value) {
            $stripped_key = substr(
                $key,
                strlen($this->metabox->getPrefix())
            );

            $formatted_value = reset($value);

            // Conditional unserialize
            if (in_array(
                $stripped_key,
                [
                    'slides',
                    'visual_styles',
                    'include_on_rugs',
                    'exclude_on_rugs',
                    'include_on_categories',
                    'exclude_on_categories',
                ]
            )) {
                $formatted_value = maybe_unserialize($formatted_value);
            }

            // Conditional default-value resetting
            if (
                in_array(
                    $stripped_key,
                    [
                        'include_on_rugs',
                        'exclude_on_rugs',
                        'include_on_categories',
                        'exclude_on_categories',
                    ]
                )
                && $formatted_value === null
            ) {
                $formatted_value = [];
            }

            $formatted[$stripped_key] = $formatted_value;
        }

        return $formatted;
    }

    /**
     * Retrieve the active offers.
     *
     * @since 1.0.0
     *
     * @param int $timestamp
     * @return array
     */
    public function getActive(int $timestamp, ?int $page_id = null, int $limit = 5): array
    {
        //    print_r(date('m/d/Y'));
        $the_query = new WP_Query([
            'post_type' => $this->post_type,
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            //TODO: Date and time functionality
            //Commented Date functionality
            // 'meta_query' => [
            //    'relation' => 'AND',

            //     [
            //         'key' => $this->metabox->addPrefix('start_date'),
            //         'value' => date('m/d/Y'),
            //         'compare' => '>=',
            //         'type' => 'NUMERIC'
            //     ],
            //     [
            //         'key' => $this->metabox->addPrefix('end_date'),
            //         'value' =>  date('m/d/Y'),
            //         'compare' => '>=',
            //         'type' => 'NUMERIC'
            //     ]

            //]


        ]);




        if (!$the_query->have_posts()) {
            return [];
        }

        $offers = [];
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $the_offer = get_post();

            // Retrieve the offer metabox data
            $offer_meta = get_post_meta($the_offer->ID);

            // Enforce proper formatting
            $offer = $this->formatOffer(
                $the_offer,
                $offer_meta
            );
            //print_r($offer);

            // Post type selection
            if (
                $page_id
                && in_array((int) $page_id, $offer["include_on_rugs"])
            ) {
                $offers[] = $offer;
                continue;
            } elseif ($page_id) {
                $child_posts = [];
                foreach ($offer["include_on_rugs"] as $parent_rug) {
                    $child_rugs = get_children($parent_rug);
                    if (!empty($child_rugs)) {
                        foreach ($child_rugs as $childrug) {

                            array_push($child_posts, $childrug->ID);
                        }
                    } else {
                        //echo 'Itself a child rug';
                    }
                }
                foreach ($offer["exclude_on_rugs"] as $exculdepost) {
                    //print_r($exculdepost);
                    if (($key = array_search($exculdepost, $child_posts)) !== false) {
                        unset($child_posts[$key]);
                    }
                }

                //Commented code of old functionality
                // $post_parents = array_reverse(get_post_ancestors($page_id));
                // $included_ids_before = array_intersect($post_parents, $offer["include_on_rugs"]);
                //$included_ids = array_diff($included_ids_before, $offer["exclude_on_rugs"]);
                //print_r($included_ids);
                if (in_array($page_id, $child_posts)) {
                    $offers[] = $offer;
                    continue;
                } else {
                    // print_r('Not included');  //commented for debugging
                }
            }


            //Get whatever object we're working with (category or post?)
            $thisObj = get_queried_object();

            //If it's a category, get the ID a different way
            if (!is_null($thisObj->term_id)) {
                $currentCatID = $thisObj->term_id;
            }
            //If it's a product, get the ID a different way
            if (!is_null($thisObj->ID)) {
                $currentproductID = $thisObj->ID;
            }

            //Category selection
            if (
                $currentCatID
                && in_array($currentCatID, $offer["include_on_categories"])
            ) {
                $offers[] = $offer;
                continue;
            } elseif ($currentCatID || is_product_category($currentCatID) || is_product()) {
                $child_categories = [];
                $child_products = [];
                foreach ($offer["include_on_categories"] as $parent_category) {

                    $child_rug_category = get_categories(array(
                        'taxonomy' => 'product_cat',
                        'parent'  => $parent_category
                    ));




                    if (!empty($child_rug_category)) {
                        foreach ($child_rug_category as $childrugcategory) {
                            // Check whether to display on product pages
                            $all_products_ids = get_posts(array(
                                'post_type' => 'product',
                                'numberposts' => -1,
                                'post_status' => 'publish',
                                'fields' => 'ids',
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'product_cat',
                                        'field' => 'term_id',
                                        'terms' => $childrugcategory->term_id, /*category name*/
                                        'operator' => 'IN',
                                    )
                                ),
                            ));
                            foreach ($all_products_ids as $productId) {
                                array_push($child_products, $productId);
                            }
                            array_push($child_categories, $childrugcategory->term_id);
                        }
                    } else {
                        //echo 'Itself a child rug';
                    }
                }


                foreach ($offer["exclude_on_categories"] as $exculdecategory) {
                    // Check whether to display on product pages
                    $all_products_ids_to_exclude = get_posts(array(
                        'post_type' => 'product',
                        'numberposts' => -1,
                        'post_status' => 'publish',
                        'fields' => 'ids',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'product_cat',
                                'field' => 'term_id',
                                'terms' => $exculdecategory, /*category name*/
                                'operator' => 'IN',
                            )
                        ),
                    ));
                    foreach ($all_products_ids_to_exclude as $productIdtoexclude) {
                        if (($key = array_search($productIdtoexclude, $child_products)) !== false) {
                            unset($child_products[$key]);
                        }
                    }
                    if (($key = array_search($exculdecategory, $child_categories)) !== false) {
                        unset($child_categories[$key]);
                    }
                }

                //Commented code of old functionality
                // $category_parents = array_reverse(get_ancestors($page_id, 'product_cat'));

                // $included_ids = array_intersect($category_parents, $offer["include_on_categories"]);
                // $included_ids = array_diff($included_ids, $offer["exclude_on_categories"]);
                if (in_array($currentCatID, $child_categories)) {
                    $offers[] = $offer;
                    continue;
                } elseif (in_array($currentproductID, $child_products)) {
                    $offers[] = $offer;
                    continue;
                } else {
                    // print_r('Not included in category'); //commented for debugging
                }
            }
        }

        wp_reset_postdata();

        return $offers;
    }

    /**
     * Retrieve the all active offers for shortcode.
     *
     * @since 1.0.0
     *
     * @param int $timestamp
     * @return array
     */
    public function getActiveShortcode(int $timestamp, ?int $page_id = null, int $limit = 5): array
    {
        //    print_r(date('m/d/Y'));
        $the_query = new WP_Query([
            'post_type' => $this->post_type,
            'post_status' => 'publish',
            'posts_per_page' => $limit,
            //TODO: Date and time functionality
            //Commented Date functionality
            // 'meta_query' => [
            //    'relation' => 'AND',

            //     [
            //         'key' => $this->metabox->addPrefix('start_date'),
            //         'value' => date('m/d/Y'),
            //         'compare' => '>=',
            //         'type' => 'NUMERIC'
            //     ],
            //     [
            //         'key' => $this->metabox->addPrefix('end_date'),
            //         'value' =>  date('m/d/Y'),
            //         'compare' => '>=',
            //         'type' => 'NUMERIC'
            //     ]

            //]


        ]);




        if (!$the_query->have_posts()) {
            return [];
        }

        $offers = [];
        while ($the_query->have_posts()) {
            $the_query->the_post();
            $the_offer = get_post();

            // Retrieve the offer metabox data
            $offer_meta = get_post_meta($the_offer->ID);

            // Enforce proper formatting
            $offer = $this->formatOffer(
                $the_offer,
                $offer_meta
            );
            //print_r($offer);

            $offers[] = $offer;
        }
        wp_reset_postdata();

        return $offers;
    }






    protected function getLabels(): array
    {
        return [
            'name' => __('Offers', 'LTS'),
            'singular_name' => __('Offer', 'LTS'),
            'menu_name' => __('LTS Offers', 'LTS'),
            'all_items' => __('All Offers', 'LTS'),
            'add_new' => __('Add Offer', 'LTS'),
            'add_new_item' => __('Add Offer', 'LTS'),
            'edit_item' => __('Edit Offer', 'LTS'),
            'new_item' => __('New Offer', 'LTS'),
            'view_item' => __('View Offer', 'LTS'),
            'view_items' => __('View Offers', 'LTS'),
            'search_items' => __('Search Offers', 'LTS'),
            'not_found' => __('No Offers Found', 'LTS')
        ];
    }

    /**
     * Register the post type(s) and taxonomies.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register()
    {
        register_post_type($this->post_type, [
            'label' => __('ffers', 'LTS'),
            'labels' => $this->getLabels(),
            'description' => '',
            'public' => true,
            'publicly_queryable' => true,
            'show_ui' => true,
            'show_in_rest' => true,
            'has_archive' => false,
            'show_in_menu' => true,
            'show_in_nav_menus' => true,
            'delete_with_user' => false,
            'exclude_from_search' => true,
            'capability_type' => 'post',
            'hierarchical' => false,
            'rewrite' => false,
            'menu_icon' => 'dashicons-megaphone',
            'supports' => [
                'title',
                'revisions',
            ],
        ]);

        register_taxonomy('occasions', 'lts_offer', [
            'labels' => [
                'name' => __('Occasions', 'LTS'),
                'singular_name' => __('Occasion', 'LTS'),
                'search_items' =>  __('Search Occasions', 'LTS'),
                'all_items' => __('All Occasions', 'LTS'),
                'parent_item' => __('Parent Occasion', 'LTS'),
                'parent_item_colon' => __('Parent Occasion:', 'LTS'),
                'edit_item' => __('Edit Occasion', 'LTS'),
                'update_item' => __('Update Occasion', 'LTS'),
                'add_new_item' => __('Add New Occasion', 'LTS'),
                'new_item_name' => __('New Occasion Name', 'LTS'),
                'menu_name' => __('Occasions', 'LTS'),
            ],
            'rewrite' => false,
            'hierarchical' => true,
        ]);
    }
}
