<?php
// Get The Offer Fields
$offer_fields = get_offer_fields_for_template($offer);
//echo '<pre>' . var_dump($offer_fields) . '</pre>';

// Get The Offer Styles
$offer_styles = get_styles($offer_fields, $groups = ['margin', 'border', 'padding']);
$text_styles = get_custom_styles($offer_fields, 'text');
$background_styles = get_custom_styles($offer_fields, 'background');
$offer_styles = $offer_styles .= $background_styles;

// Store The Data In Local Variables
$headline = isset($offer_fields[OFFERS_META_PREFIX . 'headline']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'headline']) : '';
$description = isset($offer_fields[OFFERS_META_PREFIX . 'description']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'description']) : '';
$end_date = isset($offer_fields[OFFERS_META_PREFIX . 'end_date']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'end_date']) : '';
$rug_page_url = isset($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) : '';
$buy_online_url = isset($offer_fields[OFFERS_META_PREFIX . 'buy_online_url']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'buy_online_url']) : '';
$button_text = isset($offer_fields[OFFERS_META_PREFIX . 'button_text']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'button_text']) : 'Get Offer';
?>

<div class="offer text-image" style="<?php if (isset($offer_styles)) : echo $offer_styles;
                                        endif; ?>;display:block;margin-top: 30px;position:relative">
    <div class="image-container">
        <?php $image_background_src = isset($offer_fields[OFFERS_META_PREFIX . 'single_image']) ? $offer_fields[OFFERS_META_PREFIX . 'single_image'] : PLACEHOLDER_IMAGE; ?>
        <?php if ($image_background_src != '') { ?>
            <div class="image" style="background-image: url('<?php echo esc_html($image_background_src); ?>');height: 335px;background-size: cover;"></div>
        <?php } else { ?>
            <div class="image" style="<?php echo $background_styles ?>;height: 335px;"></div>
        <?php } ?>
    </div>
    <div>
        <div class="text" style="<?php if (isset($offer_styles)) : echo $offer_styles;
                                    endif; ?>;position: absolute;top: 50%;left: 50%;transform: translate(-50%, -50%);text-align: center;padding:20px">
            <h3 class="headline" style="<?php if (isset($text_styles)) : echo $text_styles;
                                        endif; ?>;font-size: 25px;">
                <?php echo $headline; ?>
            </h3>

            <div class="description" style="<?php if (isset($text_styles)) : echo $text_styles;
                                            endif; ?>">
                <?php echo $description; ?>
            </div>
            <!-- TODO : End date functionality -->
            <!--             
            <div class="end-date" style="<?php if (isset($text_styles)) : echo esc_html($text_styles);
                                            endif; ?>">
                <?php echo $end_date; ?>
            </div> -->

            <!-- Button if exists then show -->
            <?php if (!empty($rug_page_url)) { ?>
                <a href="<?php echo $rug_page_url; ?>"><button class="button primary">Showroom Rugs</button></a>
            <?php } ?>
            <?php if (!empty($buy_online_url)) { ?>
                <a href="<?php echo $buy_online_url; ?>"><button class="button primary">Buy Online</button></a>
            <?php } ?>
        </div>
    </div>



</div>