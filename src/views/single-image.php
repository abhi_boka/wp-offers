

<?php
    // Get The Offer Fields
    $offer_fields = get_offer_fields_for_template($offer);
    //print_r($offer_fields);
    // Get The Offer Styles
    $offer_styles = get_styles($offer_fields, $groups=['margin', 'border', 'padding']);
    $text_styles = get_custom_styles($offer_fields, 'text');
    $background_styles = get_custom_styles($offer_fields, 'background');
    $offer_styles = $offer_styles .= $background_styles;

    // Store The Data In Local Variables
    $placeholder_image = 'https://placehold.it/500x500';
    $image_background_src = isset($offer_fields[OFFERS_META_PREFIX . 'single_image']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'single_image']) : PLACEHOLDER_IMAGE;
    $headline = isset($offer_fields[OFFERS_META_PREFIX . 'headline']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'headline']) : '';
    $rug_page_url = isset($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) ? esc_html($offer_fields[OFFERS_META_PREFIX . 'rug_page_url']) : '';
?>

<div class="offer single-image" style="margin-top: 30px;">
    <div class="offer-inner-single-image">
        <a href="<?php echo $rug_page_url; ?>">
        
            <div class="image" style="background-image: url('<?php echo $image_background_src; ?>');height: 335px;background-size: cover;background-repeat: no-repeat;"></div>
        
        </a>
    </div>
</div>