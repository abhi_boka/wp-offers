<?php

/**
 * WordPress Offers.
 *
 * @since 1.0.0
 * @package WordPress Offers
 * @copyright 2021 Lacey Tech
 * @link https://lacey-tech.com
 */

namespace LTS\Offers;

use CMB2;

/**
 * Offers system Metabox registration and handling.
 *
 * @since 1.0.0
 * @author Dom Webber <dom.webber@hotmail.com>
 */
class Metabox
{

    /**
     * The preview display callback.
     *
     * @since 1.0.0
     *
     * @var Display
     */
    protected $display_callback;
    /**
     * The metabox prefix.
     *
     * @since 1.0.0
     *
     * @var string
     */
    protected $prefix;

    protected const BRAND_COLOUR_PALETTE = [
        '#922333', # Rugmart Red
        '#fcb900', # Rugmart Yellow
    ];

    protected const TEXT_COLOUR_PALETTE = [
        '#333333', # Lighter Black
        '#FFFFFF', # White,
        '#5A4693', # Light Purple
        '#31273B', # Dark Purple
    ];

    protected const BACKGROUND_COLOUR_PALETTE = [
        '#FF9A00', # Orange
        '#4caf50', # Green
        '#689EB8', # Light Blue
        '#A79B94', # Grey/Brown
        '#768591', # Blue/Grey
        '#5A4693', # Light Purple
        '#31273B', # Dark Purple
        '#FE0089', # Pink
        '#CCBCA5', # Tan
    ];

    /**
     * Constructor.
     *
     * @since 1.0.0
     *
     * @param string $prefix
     */
    public function __construct(
    
        callable $display_callback,
        string $prefix = ''
    ) {
     
        $this->prefix = $prefix;
        $this->display_callback = $display_callback;
    }

    /**
     * Retrieve the meta prefix.
     *
     * @since 1.0.0
     *
     * @return string
     */
    public function getPrefix(): string
    {
        return $this->prefix;
    }

    /**
     * Add the prefix to a given string.
     *
     * @since 1.0.0
     *
     * @param string $string
     * @return string
     */
    public function addPrefix(string $string): string
    {
        return $this->prefix . $string;
    }

    /**
     * Register the metaboxes through CMB2.
     *
     * @since 1.0.0
     *
     * @return void
     */
    public function register(
        CMB2 $details_box,
        CMB2 $display_options_box,
        //CMB2 $preview_box, //Removing Offer Preview Box
        CMB2 $styles_box
    ) {
        $this->registerDetails($details_box);
        $this->registerDisplayOptions($display_options_box);
        //$this->registerPreview($preview_box); //Removing Offer Preview Box
        $this->registerStyles($styles_box);
        return;
    }

    protected function registerDisplayOptions(CMB2 $display_options_box)
    {
        $display_options_box->add_field([
            'name' => __('Offer Display Options'),
            'id' => $this->addPrefix('type'),
            'type' => 'select',
            'show_options_none' => true,
            'options' => [
                'single-image' => 'Single Image Banner',
                'text' => 'Text Only',
                'text-image' => 'Text Image',
                //TODO : Coming into next phase
                // 'countdown-timer' => 'Countdown Timer (Coming Soon)'
            ],
        ]);

        $display_options_box->add_field([
            'name' => __('Offer Headline'),
            'id' => $this->addPrefix('headline'),
            'type' => 'text',
            'desc' => __(''),
            'attributes' => array(
                'required'            => true, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['text', 'text-image'])
            ),
        ]);

        $display_options_box->add_field([
            'name' => __('Short Offer Description'),
            'id' => $this->addPrefix('description'),
            'type' => 'textarea',
            'attributes' => array(
                'required'            => true, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['text', 'text-image'])
            ),
            'options' => [
                'wpautop' => true, // use wpautop?
                'media_buttons' => false, // show insert/upload button(s)
                'rows' => 3,
                'teeny' => true, // output the minimal editor config
            ],
            'desc' => __('Briefly Describe The Offer. This is used on Text, Text + Image and Text + Gallery Offer Display options.'),
        ]);


        $display_options_box->add_field([
            'name' => __('Rug Page URL'),
            'id' => $this->addPrefix('rug_page_url'),
            'type' => 'text_url',
            'desc' => __('Add the rug page link where visitors should go when clicking the button'),
            'attributes' => array(
                'required'            => false, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['text', 'text-image', 'single-image'])
            ),
        ]);

        $display_options_box->add_field([
            'name' => __('Buy Online URL'),
            'id' => $this->addPrefix('buy_online_url'),
            'type' => 'text',
            'desc' => __('Add the buy online link you want to use for the buy online button'),
            'attributes' => array(
                'required'            => false, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['text', 'text-image'])
            ),
        ]);


        $display_options_box->add_field(array(
            'name'    => 'Offer Image',
            'desc'    => 'Upload an image and show it next to the offer text',
            'id'      => $this->addPrefix('single_image'),
            'type'    => 'file',
            'attributes' => [
                'required'            => true, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['single-image', 'text-image'])
            ],
            'options' => ['url' => false],
            'text'    => ['add_upload_file_text' => 'Open From Media Library'],
            'query_args' => [
                'type' => ['image/gif', 'image/jpg', 'image/png'],
            ],
            'preview_size' => 'medium', // Image size to use when previewing in the admin.
        ));
    }


    protected function registerDetails(CMB2 $details_box): CMB2
    {



        // Commented The date boxes
        // $details_box->add_field([
        //     'name' => __('Start date', 'cmb2'),
        //     'desc' => __('The offer start date.', 'cmb2'),
        //     'id' => $this->addPrefix('start_date'),
        //     'type' => 'text_date',
        // ]);

        // $details_box->add_field([
        //     'name' => __('End date', 'cmb2'),
        //     'desc' => __('The offer end date.', 'cmb2'),
        //     'id' => $this->addPrefix('end_date'),
        //     'type' => 'text_date',
        // ]);

        //Implemented multiselect
        $posts_rugs = [];
        $args = array(
            'post_type'    => 'rugs',
            'post_status' => 'publish',
            'posts_per_page' => -1
        );
        $rug_posts = get_posts($args);


        foreach ($rug_posts as $rug_post) {
            $posts_rugs[$rug_post->ID] = $rug_post->post_title;
        }

        $details_box->add_field([
            'name' => __('Include rugs', 'cmb2'),
            'desc' => __('Select the rugs that this offer should be displayed on. Child rugs will also be selected.', 'cmb2'),
            'id' => $this->addPrefix('include_on_rugs'),
            'type' => 'pw_multiselect',
            'options' => $posts_rugs,
            'attributes' => array(
                'placeholder' => '  Please select rugs.'
            ),
            // 'limit' => 2500,
            // 'sortable' => true,
            // 'query_args' => [
            //     'post_type' => ['rugs'],
            //     'post_status' => ['publish'],
            //     'posts_per_page' => 5
            // ],
            // 'attributes' => [
            //     'placeholder' => 'Find rugs to include'
            // ]
        ]);

        $details_box->add_field([
            'name' => __('Exclude rugs', 'cmb2'),
            'desc' => __('Select the child rugs to exclude.'),
            'id' => $this->addPrefix('exclude_on_rugs'),
            'type' => 'pw_multiselect',
            'options' => $posts_rugs,
            'attributes' => array(
                'placeholder' => '  Please select rugs.'
            ),
            // 'limit' => 2500,
            // 'sortable' => true,
            // 'query_args' => [
            //     'post_type' => ['rugs'],
            //     'post_status' => ['publish'],
            //     'posts_per_page' => 5
            // ],
            // 'attributes' => [
            //     'placeholder' => 'Find pages to exclude'
            // ]
        ]);

        $cats = [];
        $tmp_cats = get_terms('product_cat', [
            'orderby' => 'name',
            'order' => 'asc',
            'hide_empty' => false
        ]);

        foreach ($tmp_cats as $category) {
            $cats[$category->term_id] = $category->name;
        }
        
        $details_box->add_field([
            'name' => __('Include Buy Online', 'cmb2'),
            'desc' => __('Select the rug categories to include. Child categories will also be included.'),
            'id' => $this->addPrefix('include_on_categories'),
            'type' => 'pw_multiselect',
            'options' => $cats
        ]);
        
        $details_box->add_field([
            'name' => __('Exclude Buy Online', 'cmb2'),
            'desc' => __('Select the child rug categories to exclude.'),
            'id' => $this->addPrefix('exclude_on_categories'),
            'type' => 'pw_multiselect',
            'options' => $cats,
            
        ]);








        return $details_box;
    }

    function registerStyles(CMB2 $styles_box): CMB2
    {
        $styles_box->add_field([
            'name'    => 'Text Colour',
            'id'      => $this->addPrefix('text_colour'),
            'type'    => 'colorpicker',
            'default' => '#333333',
            'attributes' => [
                'data-colorpicker' => json_encode([
                    'palettes' => self::BRAND_COLOUR_PALETTE + self::TEXT_COLOUR_PALETTE,
                ]),
            ],
        ]);

        $styles_box->add_field([
            'name'    => 'Background Colour',
            'id'      => $this->addPrefix('background_colour'),
            'type'    => 'colorpicker',
            'default' => '#E3E4E7',   # Light Gray
            'attributes' => [
                'required'            => true, // Will be required only if visible.
                'data-conditional-id' => $this->addPrefix('type'),
                'data-conditional-value' => wp_json_encode(['text', 'text-image']),
                'data-colorpicker' => json_encode([
                    'palettes' => self::BRAND_COLOUR_PALETTE + self::BACKGROUND_COLOUR_PALETTE,
                ]),
            ],
        ]);

        $styles_box->add_field([
            'id'            => $this->addPrefix('visual_styles'),
            'type'          => 'visual_style_editor',
            'hide_border'   => false,
            'hide_padding'   => true,
            'hide_text_options'   => true,
            'hide_background_options'   => true,
            //'hide_border_options'   => true,
        ]);

        return $styles_box;
    }

    protected function registerPreview(CMB2 $preview_box): CMB2
    {
        $preview_box->add_field([
            'name' => __('Preview'),
            'id' => $this->addPrefix('preview'),
            'type' => 'text',
            //'desc' => __('This is a preview of how the offer will display on the website when published'),
            'save_field'  => false,    // Otherwise CMB2 will end up removing the value.
            'before_row' => $this->display_callback,
            'show_names' => false,
            'attributes'  => [
                'hidden' => 'hidden'
            ]
        ]);

        return $preview_box;
    }
}
